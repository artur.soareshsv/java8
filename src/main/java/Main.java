import java.time.*;
import java.time.format.DateTimeFormatter;


public class Main {
    public static void main(String[] args) {
        LocalDate hoje = LocalDate.now();
        System.out.println("Data de hoje: " + hoje);

        LocalDate olimpiadasRio = LocalDate.of(2019, Month.JUNE, 6);

        int anos = olimpiadasRio.getYear() - hoje.getYear();
        System.out.println("Faltam " + anos + " anos para as oliampiadas do Rio de Janeiro.");

        Period period = Period.between(hoje, olimpiadasRio);
        System.out.println("Faltam " + period.getDays() + " dias para as oliampiadas do Rio de Janeiro.");

        LocalDate proximasOlimpiadas = olimpiadasRio.plusYears(4);
        System.out.println("Próximas olimpiadas: " + proximasOlimpiadas);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("Formatação de data: " + proximasOlimpiadas.format(formatter));

        DateTimeFormatter formatterWithTime = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");
        LocalDateTime agora = LocalDateTime.now();
        System.out.println("Agora: " + agora.format(formatterWithTime));

        LocalTime intervalo = LocalTime.of(15, 30);
        System.out.println("Horário do Intervalo: " + intervalo);
    }
}
